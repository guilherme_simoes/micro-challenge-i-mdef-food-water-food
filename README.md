# **Micro Challenge I - Food Water Food**

A fun and interactive card game about the water footprint of common foodstuffs.

# **Initial Idea/Project Concept:**
In the beginning, we were looking to create a complex board game at the intersection of our areas of interest & research, food and water. However, since we only had 2 days, this proved out to be infeasible and unrealistic. Therefore, we opted for a simpler card game.

The initial idea was to develop an educational game that helps to think differently about water footprint of various food items, broadens food-water literacy, and encourages responsible consumption. The results are a competitive point collection game.

Here below are the instructions:

FOOD WATER FOOD

Players: 2-4

For All Ages

Instructions:

The amount of food you need to guess the water footprint for is 1KG. This means that in case of coffee, it's 1KG of dry coffee, if it's a liquid, like olive oil, it's 1kg of the liquid (around or a bit less than 1L).

Each player gets a water footprint indicator. Place deck of cards facing food side up to begin!
Each round consists of guessing the amount of water needed to produce the food item on the card by sliding the water footprint indicator. Make sure other players can’t see yours! Then, when everybody made their guess, revealing them at the same time. After revealing, flip the card on top to check it’s actual water footprint. Evaluate the players cards against the actual number, the player who made the closest guess gets a WaterCoin. If two or more players made the same close or correct guess, they all get a WaterCoin. When the deck of cards run out, the player with the most water coin wins. Good Luck!  

# **What is Supposed To Do Or Not To Do:**
What is Supposed To Do: This game is supposed to promote water literacy in children, to give them a better overall understanding of the environmental emergency currently happening around them and bound to get extremely critical a billion seconds from now (if we continue _business as usual_).

What is Not Supposed To Do: It's not supposed to solve or help solve any of the problems that constitute the environmental emergency. This game is only meant to stimulate the player's critical thinking and creativity, and maybe to inspire them to potentially become changemakers. It's all up to the next generation, they are the future...

# **How We Planned and Executed The Project:**
We divided the different tasks among us. Me, Guilherme, did the research and data collection to obtain the values of the water footprint for the different foodstuffs. Mark did the design and conceptualization of the game artifacts (Cards and WaterCoins). To make the elements for this game Flaticon (http://flaticon.com/) was used. Later on the laser cutting machine was used to carve them out from the cardbox sheet.

# **Data Used:**
![Water footprint table](Pictures/datasheet.png)

We used data from waterfootprint.org, there is an openly downloadable PDF with all the data we used to make this game: https://waterfootprint.org/media/downloads/Mekonnen-Hoekstra-2011-WaterFootprintCrops.pdf

The above data sheet contains the water footprint of 30 different foodstuffs, represented by the cards. The players use the slider to give an estimative/approximation of that value. The player who gets closer to this value, or, even better, _hits the jackpot_, gets a WaterCoin. The player with the most accumulated WaterCoins becomes the winner!

# **System Diagram:**

![System Diagram](Pictures/System_Diagram.png)

# **Design Elements:**

![graphics](Pictures/front.jpeg)

The design is included in the files. Print out the file FOODWATERFOOD.PDF in A3 format from pages 1-10.

# **Fabrication Process and BOM (Build of Materials):**
MATERIALS:

Carboard Paper (1.25mm) - Cards, Slider, Coins

Plastic Sheet, or White Cardboard (Printable) - Box

Vinyl - Matte Black, Mountable

Paper Fasteners, Glue

![fasteners](Pictures/fasteners.jpg)

MANUFACTURING STEPS:

1. Print out the card, slider and coin elements, double sided on A3 cardboard sheets (page1-9), while printing out the box layout on a printable A3 plastic sheet (page 10).
2. Laser cut, or use scissors/box cutter to cut the paper sheets carefully
3. Assembly the indicators as seen in the image below using glue in the edges and a paper fastener in the middle. Make sure that the glue does not touch the rotating wheel in the middle, and it is not too tightly fastened.
![assembly](Pictures/assembly.png)
4. Optional - to finish up, using black matte vinyl, vinyl cut the box decoration and sealing stickers and mount it on the front, and box openings:
![Vinyl Mount](Pictures/vinylmount.png)

# **Design & Fabrication Files:**

[Printable PDF](https://gitlab.com/guilherme_simoes/micro-challenge-i-mdef-food-water-food/-/blob/master/Files/FOODWATERFOOD_FINAL.pdf)

[Adobe Illustrator File](https://gitlab.com/guilherme_simoes/micro-challenge-i-mdef-food-water-food/-/blob/master/Files/FOODWATERFOOD_FINAL.ai)

[Rhinoceros 3D Files for Laser Cutting - Rhino7](https://gitlab.com/guilherme_simoes/micro-challenge-i-mdef-food-water-food/-/blob/master/Files/FoodWater_Lasercut_final.3dm)

[Vinyl Cutting - DXF](https://gitlab.com/guilherme_simoes/micro-challenge-i-mdef-food-water-food/-/blob/master/Files/FoodWater_VINYL_2.dxf)

[Flaticon Collection](https://gitlab.com/guilherme_simoes/micro-challenge-i-mdef-food-water-food/-/blob/master/mycollection.zip)

# **Photographs of the End Artifacts:**

![box](Pictures/box1.jpeg)
![box](Pictures/box2.jpeg)
![box](Pictures/box3.jpeg)
![game](Pictures/game1.jpeg)

# **Iteration Process:**

##### Before we even started, the most time-consuming process was actually deciding what kind of game we would be doing. We first thought of an interactive board game, where you started with a small agricultural land plot, and you gradually progressed by getting/acquiring more knowledge on how to farm the land. Basically understanding processes such as irrigation, fertilization, pesticide/insecticide/fungicide application, harvesting, etc., and also being aware of the energy, water and CO2 footprint of your decisions and actions while playing the game. But, as you can well imagine, this type of game would be too complex and difficult to execute (at least for the timeline of 1 week). So instead we opted to build a card game. The next challenge was to decide the aim/objective and the rules of this game. It took some intense and exhausting hours of brainstorming, but eventually we reached a final conclusion on that too. The rest of the process was fairly easy and smooth, with a few exceptions... We had to go twice to the printing shop, since we had quite a few measurement mistakes/lapses when doing the sketching and designing for the game's artifact elements using Rhinoceros 3D. But we redid it, and got that right in the end too.        

# **Bibliography:**
https://healabel.com/
https://www.bbc.com/news
https://www.theguardian.com/international
https://www.businessinsider.es/
https://waterfootprint.org/en/
https://waterfootprint.org/media/downloads/Mekonnen-Hoekstra-2011-WaterFootprintCrops.pdf
https://waterfootprint.org/en/resources/waterstat/product-water-footprint-statistics/
https://evgenii.com/water-footprint/en/
https://static.ewg.org/reports/2011/meateaters/pdf/methodology_ewg_meat_eaters_guide_to_health_and_climate_2011.pdf
https://www.visualcapitalist.com/visualising-the-greenhouse-gas-impact-of-each-food/
https://ourworldindata.org/food-choice-vs-eating-local

# **Listed Future Development Opportunities for this Project:**
Future Development Opportunities:
 * 1. Board Game
 * 2. Educational Game
 * 3. Environmental/Eco-Friendly App

# **Link to Website:**
https://wordpress.com/page/sassyduckweedfarm.wordpress.com/5

